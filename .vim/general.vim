"""""""""""""""""""""""""""
" GENERAL SETTINGS
"""""""""""""""""""""""""""

" Theme
set background=dark
set termguicolors     " enable true colors support
colorscheme onedark

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

" syntax enable line is already set by vim-plug
syntax enable	" enable syntax processing
" filetype plugin indent on is already set by vim-plug
filetype plugin indent on

if !has('nvim')
  set nocompatible " don't try be vi compatible
endif
set showtabline=2  " Show tabline
set guioptions-=e  " Don't use GUI tabline
set number " show line numbers
set relativenumber " show relative line numbers
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu	" visual autocomplete for command menu
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
set visualbell
set cursorline	" highlight current line
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set undofile

if !has('nvim')
  set mouse=a
endif
set title " show title in terminal title bar
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab " use spaces instead of tabs.
set smarttab " let tab key insert 'tab stops', and bksp deletes tabs.
set smartindent

" Use system clipboard
if !has('nvim')
set clipboard=unnamedplus
endif

set showcmd
set noswapfile " Do not create .swp files

" change cursor shape in different modes
" let &t_SI = "\<Esc>[6 q"
" let &t_SR = "\<Esc>[4 q"
" let &t_EI = "\<Esc>[2 q"

" highlight json files
" autocmd BufNewFile,BufRead *.json set ft=javascript

" Centralize backups, swapfiles and undo history
set backupdir=~/.vim/backups
set directory=~/.vim/swaps
if exists("&undodir")
  set undodir=~/.vim/undo
endif

set ignorecase
set smartcase
" set gdefault
set incsearch	" search as characters are entered
set showmatch	" highlight matching [{()}]
set hlsearch	" highlight search matches

set showbreak=↪\
if has('nvim')
  set list
endif
" set listchars=tab:▸\ ,eol:¬, space:.
" set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨,space:.

" open new splits to right and bottom
set splitbelow
set splitright

" set initial foldlevel to open on read file
set foldlevel=99

" turn off auto comment insertion
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" interactive substitution command
set inccommand=split

" change foldmethod for pug
autocmd Filetype pug setlocal foldmethod=indent

autocmd BufNewFile,BufRead *.js set ft=javascript
autocmd BufNewFile,BufRead *.json set ft=javascript
autocmd BufNewFile,BufRead *.js.liquid set ft=javascript



" abbreviations
cabbrev PI PlugInstall
cabbrev PU PlugUpdate

function! Reg()
  reg
  echo "Register: "
  let char = nr2char(getchar())
  if char != "\<Esc>"
    execute "normal! \"".char."p"
  endif
  redraw
endfunction

command! -nargs=0 Reg call Reg()
