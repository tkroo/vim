"""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')


Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/vim-peekaboo'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'itchyny/lightline.vim'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-commentary'
Plug 'vim-syntastic/syntastic'
Plug 'Yggdroot/indentLine'
"Plug 'junegunn/vader.vim'
Plug 'gangleri/vim-diffsaved'
"Plug 'mbbill/undotree'
"Plug 'tomtom/tcomment_vim'
"
"
"
" Plug 'jiangmiao/auto-pairs'

" Plug 'junegunn/goyo.vim'
" Plug 'tpope/vim-surround'
Plug 'terryma/vim-multiple-cursors'

" post install (yarn install | npm install)
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
Plug 'mattn/emmet-vim', { 'for': ['html','css'] }



if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

" JAVASCRIPT STUFF

"Plug 'othree/yajs.vim'
"Plug '1995eaton/vim-better-javascript-completion'
"Plug 'gavocanov/vim-js-indent'
"Plug 'ternjs/tern_for_vim', { 'for': ['javascript', 'javascript.jsx'] }
":w
"Plug 'carlitux/deoplete-ternjs', { 'for': ['javascript', 'javascript.jsx'] }
"Plug 'elzr/vim-json'
"Plug 'posva/vim-vue'



" syntax highlighting
" autocmd BufNewFile,BufRead *.pug setf pug
Plug 'https://github.com/digitaltoad/vim-pug.git',  { 'for': 'pug' }
Plug 'tpope/vim-liquid', { 'for': 'liquid' } 

Plug 'vim-scripts/loremipsum'
" colorschemes
Plug 'drewtempelmeyer/palenight.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'rakr/vim-one'
Plug 'tomasr/molokai'
Plug 'ayu-theme/ayu-vim'
Plug 'morhetz/gruvbox'
Plug 'cocopon/iceberg.vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'nanotech/jellybeans.vim'



call plug#end()
"""""""""""""""""""""""""""


