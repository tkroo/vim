"""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Plug 'junegunn/vim-peekaboo'
" :let g:peekaboo_compact=1
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'itchyny/lightline.vim'
Plug 'mgee/lightline-bufferline'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-commentary'
" Plug 'tomtom/tcomment_vim'


Plug 'ap/vim-buftabline'



" Plug 'vim-syntastic/syntastic'

Plug 'Yggdroot/indentLine'

Plug 'jiangmiao/auto-pairs'
Plug 'gangleri/vim-diffsaved'
Plug 'godlygeek/tabular'
" Plug 'junegunn/vader.vim'
" Plug 'mbbill/undotree'
" Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-surround'
" Plug 'tpope/vim-unimpaired'
" Plug 'terryma/vim-multiple-cursors'
Plug 'ervandew/supertab'
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
Plug 'mattn/emmet-vim', { 'for': ['html','css'] }
Plug 'machakann/vim-highlightedyank'

" syntax highlighting
Plug 'sheerun/vim-polyglot' " adds on demand support for lots of languages
" Plug 'https://github.com/digitaltoad/vim-pug.git',  { 'for': 'pug' }
" Plug 'tpope/vim-liquid', { 'for': 'liquid' } 

" JAVASCRIPT STUFF
Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
Plug 'ternjs/tern_for_vim', { 'do': 'npm install', 'for': ['javascript'] }
Plug 'pangloss/vim-javascript'
Plug '1995eaton/vim-better-javascript-completion'
Plug 'othree/yajs.vim', { 'for': 'javascript' }
" Plug 'othree/es.next.syntax.vim', { 'for': 'javascript' }
" Plug 'elzr/vim-json'
" Plug 'posva/vim-vue'

Plug 'vim-scripts/loremipsum'
" Plug 'mhinz/vim-startify'

" colorschemes
Plug 'chriskempson/base16-vim'
Plug 'joshdick/onedark.vim'
Plug 'tomasr/molokai'
Plug 'morhetz/gruvbox'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'challenger-deep-theme/vim', { 'as': 'challenger-deep' }
" Plug 'rakr/vim-one'
" Plug 'mhinz/vim-janah'
" Plug 'altercation/vim-colors-solarized'
" Plug 'ayu-theme/ayu-vim'
" Plug 'cocopon/iceberg.vim'
" Plug 'dracula/vim', { 'as': 'dracula' }
" Plug 'nanotech/jellybeans.vim'


call plug#end()
"""""""""""""""""""""""""""

" Plugin specific settings


let g:ascii = ['']
" let g:startify_custom_header = g:ascii + startify#fortune#boxed()





" FZF
" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})

" deoplete stuff
let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
let g:deoplete#sources#ternjs#tern_bin = '/home/david/.nvm/versions/node/v10.11.0/bin/tern'
let g:deoplete#enable_at_startup = 1
" autocmd InsertEnter * call deoplete#enable()


" let g:syntastic_vim_checkers = ['vint']

" indent guides settings
" let g:indentLine_setColors = 0
" let g:indentLine_color_term = 239
let g:indentLine_char = '┊'

" gitgutter settings
let g:gitgutter_terminal_reports_focus=0
let g:gitgutter_max_signs = 300 " 500 is default
let g:gitgutter_highlight_lines = 0  " Turn off gitgutter highlighting
" let g:gitgutter_realtime = 1
" let g:gitgutter_eager = 0

 autocmd InsertLeave *.json setlocal concealcursor=inc
