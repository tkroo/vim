set nocompatible
"syntax enable
set encoding=utf-8

" source plugins and plugin specific settings
source ~/.vim/plugins.vim

" source general settings
source ~/.vim/general.vim

" source key mappings
source ~/.vim/keymappings.vim

" source lightline settings
source ~/.vim/lightline.vim
